#!/bin/sh

url='https://hub.docker.com/v2/repositories/rundeck/rundeck/tags?page_size=10000'


versions=$(wget -q "${url}" -O - | \
        grep -oe '"name":"[0-9.]\+"' | \
        cut -d '"' -f 4 | \
        sort -V)

#>&2 echo "Versions: ${versions}"

latest_version=$(echo "${versions}" | tail -n 1)

>&2 echo "Latest Version: ${latest_version}"

minor_versions=$(echo "${versions}" | cut -d '.' -f 1-2 | sort -u | xargs)

>&2 echo "Minor Versions: ${minor_versions}"

bugfix_versions=""

for minor_version in ${minor_versions}; do
        latest_minor_version=$(echo "${versions}" | \
                grep "${minor_version}" | \
                sort -V| \
                tail -n 1)
        if [ -z "${bugfix_versions}" ]; then
                bugfix_versions="${latest_minor_version}"
        else
                bugfix_versions="${bugfix_versions} ${latest_minor_version}"
        fi
done

>&2 echo "Bugfix Versions: ${bugfix_versions}"

for bugfix_version in ${bugfix_versions}; do
        echo "${bugfix_version}"
done | tac | xargs
