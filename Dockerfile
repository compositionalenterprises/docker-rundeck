FROM rundeck/rundeck:latest

USER root
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update && \
    apt-get install -y \
        git \
        python3.6 \
        libffi-dev \
        libssl-dev \
        python3.6-dev && \
    unlink /usr/bin/python3 && \
    ln -sT /usr/bin/python3.6 /usr/bin/python3 && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    curl https://bootstrap.pypa.io/get-pip.py | python3.6

USER rundeck
